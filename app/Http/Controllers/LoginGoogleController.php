<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Google\Service\Calendar;
use Google\Service\Calendar\Event;
use Google_Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LoginGoogleController extends Controller
{
    //
    public function index()
    {
        $data = [];
        if (session()->get('google_access_token')) {
            $client = new Google_Client();
            $client->setAuthConfig(Storage::path('client_secret.json'));
            $client->setScopes(Calendar::CALENDAR);
            $client->setAccessToken(session()->get('google_access_token'));

            $service = new Calendar($client);
            // Print the next 10 events on the user's calendar.
            $calendarId = 'primary';
            $optParams = array(
                'maxResults' => 10,
                'orderBy' => 'startTime',
                'singleEvents' => true,
                'timeMin' => date('c'),
            );
            $results = $service->events->listEvents($calendarId, $optParams);
            $events = $results->getItems();
            if ($events) {
                foreach ($events as $event) {
                    $start = $event->start->dateTime;
                    if (empty($start)) {
                        $start = $event->start->date;
                    }
                    $carbon  = new Carbon($start);
                    $data[] = $event->getSummary() . " | " . $carbon->toDateTimeString();
                }
            }
        }
        return view('index', [
            'data' => $data,
            'access_token' => session()->get('google_access_token') ? json_encode(session()->get('google_access_token')) : null
        ]);
    }

    public function login()
    {
        // $client = new Google_Client();
        // $client->setAuthConfig(Storage::path('client_secret.json'));
        // $client->setScopes(Calendar::CALENDAR);
        // $client->setPrompt('select_account consent');
        return redirect(route('oauthCallback'));
    }

    public function createEvent()
    {
        if (session()->get('google_access_token')) {
            $client = new Google_Client();
            $client->setAuthConfig(Storage::path('client_secret.json'));
            $client->setScopes(Calendar::CALENDAR);
            $client->setAccessToken(session()->get('google_access_token'));

            $service = new Calendar($client);
            $event = new Event(array(
                'summary' => 'Google I/O 2015',
                'location' => '800 Howard St., San Francisco, CA 94103',
                'description' => 'A chance to hear more about Google\'s developer products.',
                'start' => array(
                    'dateTime' => '2015-05-28T09:00:00-07:00',
                    'timeZone' => 'America/Los_Angeles',
                ),
                'end' => array(
                    'dateTime' => '2015-05-28T17:00:00-07:00',
                    'timeZone' => 'America/Los_Angeles',
                ),
                'attendees' => array(
                    array('email' => 'lpage@example.com'),
                    array('email' => 'sbrin@example.com'),
                ),
                'reminders' => array(
                    'useDefault' => FALSE,
                    'overrides' => array(
                        array('method' => 'email', 'minutes' => 24 * 60),
                        array('method' => 'popup', 'minutes' => 10),
                    ),
                ),
            ));

            $calendarId = 'primary';
            $event = $service->events->insert($calendarId, $event);
            var_dump($event->htmlLink);
            exit;
        } else {
            return redirect(route('index'));
        }
    }

    public function oauthCallback()
    {
        $client = new Google_Client();
        $client->setAuthConfig(Storage::path('client_secret.json'));
        $client->setScopes(Calendar::CALENDAR);
        $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/google-calendar/oauth-callback');
        if (!isset($_GET['code'])) {
            $auth_url = $client->createAuthUrl();
            return redirect(filter_var($auth_url, FILTER_SANITIZE_URL));
        } else {
            $client->authenticate($_GET['code']);
            session()->put('google_access_token', $client->getAccessToken());
            return redirect(route('index'));
        }
    }
}
