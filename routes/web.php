<?php

use App\Http\Controllers\LoginGoogleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/google-calendar', [LoginGoogleController::class, 'index'])->name('index');
Route::get('/google-calendar/login', [LoginGoogleController::class, 'login'])->name('login');
Route::get('/google-calendar/oauth-callback', [LoginGoogleController::class, 'oauthCallback'])->name('oauthCallback');

Route::get('/google-calendar/create-event', [LoginGoogleController::class, 'createEvent'])->name('createEvent');

Route::get('/test', function () {
    return view('indexjs');
});
