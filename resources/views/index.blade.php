<!DOCTYPE html>
<html>
	<head>
  </head>
  <body>
    <h3>Google Calendar Quickstart</h3>
    @if ($access_token)
      <a href="{{ route('createEvent') }}">Create Event</a>
      @if ($data)
      <ul>
        @foreach ($data as $row)
          <li> {{$row}}</li>
        @endforeach
      </ul>
      @endif
    @else
      <a href="{{ route('login') }}">Login Google</a>
    @endif
  </body>
</html>